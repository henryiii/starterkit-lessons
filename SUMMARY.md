# Summary

* [The LHCb Starterkit](README.md)
  * [Contributing](CONTRIBUTING.md)

----

* [First analysis steps](first-analysis-steps/README.md)
    * [Pre-workshop checklist](first-analysis-steps/prerequisites.md)
    * [Goals of the course](first-analysis-steps/introduction-to-course.md)
    * [The LHCb data flow](first-analysis-steps/dataflow.md)
    * [Changes to the data flow in Run 2](first-analysis-steps/run-2-data-flow.md)
    * [An introduction to LHCb Software](first-analysis-steps/davinci.md)
    * [Finding data in the Bookkeeping](first-analysis-steps/bookkeeping.md)
    * [Downloading a file from the grid](first-analysis-steps/files-from-grid.md)
    * [Interactively exploring a DST](first-analysis-steps/interactive-dst.md)
    * [Fun with LoKi Functors](first-analysis-steps/loki-functors.md)
    * [Running a minimal DaVinci job locally](first-analysis-steps/minimal-dv-job.md)
    * [Running DaVinci on the grid](first-analysis-steps/davinci-grid.md)
    * [TupleTools and branches](first-analysis-steps/add-tupletools.md)
    * [How do I use DecayTreeFitter?](first-analysis-steps/decay-tree-fitter.md)
    * [More Ganga](first-analysis-steps/ganga-data.md)
    * [Storing large files on EOS](first-analysis-steps/eos-storage.md)
    * [Splitting a job into subjobs](first-analysis-steps/split-jobs.md)
    * [Developing LHCb Software](first-analysis-steps/lhcb-dev.md)
    * [Asking good questions](first-analysis-steps/asking-questions.md)
    * [Early career, gender and diversity](first-analysis-steps/ecgd.md)
    * [Contribute to this lesson](first-analysis-steps/contributing-lesson.md)

----

* [Second analysis steps](second-analysis-steps/README.md)
    * [Using git to develop LHCb software](second-analysis-steps/lb-git.md)
    * [Building your own decay](second-analysis-steps/building-decays.md)
      * [The Selection Framework](second-analysis-steps/building-decays-part0.md)
      * [A Historical Approach](second-analysis-steps/building-decays-part1.md)
      * [Modern Selection Framework](second-analysis-steps/building-decays-part2.md)
    * [What to do when something fails](second-analysis-steps/fixing-errors.md)
    * [Run a different stripping line on simulated data](second-analysis-steps/rerun-stripping.md)
    * [Replace a mass hypothesis](second-analysis-steps/switch-mass-hypo.md)
    * [Reuse particles from a decay tree](second-analysis-steps/filter-in-trees.md)
    * [The simulation framework](second-analysis-steps/simulation.md)
    * [HLT intro](second-analysis-steps/hlt-intro.md)
    * [TisTos DIY](second-analysis-steps/tistos-diy.md)
    * [Ganga Scripting](second-analysis-steps/ganga-scripting.md)
    * [Managing files in Ganga](second-analysis-steps/managing-files-with-ganga.md)

----

* [First development steps](first-development-steps/README.md)
    * [Setting up a development environment](first-development-steps/01-intro.md)
      * [Setting up a CernVM](first-development-steps/01a-cernvm.md)
      * [Setting up with Docker](first-development-steps/01b-docker.md)
      * [Setting up CentOS 7](first-development-steps/01c-centos.md)
    * [Git for LHCb](first-development-steps/01-git.md)
      * [LB git](first-development-steps/01d-setupsimple.md)
      * [Pure git](first-development-steps/01e-setupcomplete.md)
    * [Building LHCb software](first-development-steps/01f-building.md)
    * [Testing and examples in LHCb](first-development-steps/01g-testing.md)
    * [Hello World in the Gaudi Framework](first-development-steps/02a-gaudi-helloworld.md)
    * [Intro to the Gaudi Framework](first-development-steps/02b-gaudi-intro.md)
    * [The Gaudi Framework](first-development-steps/03a-gaudi.md)
    * [Upgrading algorithms](first-development-steps/03b-upgrading.md)
    * [What's new in C++](first-development-steps/05-cpp.md)
      * [What's new in C++11](first-development-steps/05a-cpp11.md)
      * [What's new in C++14](first-development-steps/05b-cpp14.md)
      * [What's new in C++17](first-development-steps/05c-cpp17.md)
      * [Prospects in C++'s future](first-development-steps/05d-cppfuture.md)
    * [C++ performance](first-development-steps/06a-perf.md)
    * [Python in the upgrade era](first-development-steps/06b-python.md)

----

 * [Download PDF](ref://starterkit-lessons.pdf)

